import matplotlib.pyplot as plt
import numpy as np
from scipy.cluster.hierarchy import dendrogram
from sklearn.datasets import make_blobs, make_circles, make_moons
from sklearn.cluster import KMeans, AgglomerativeClustering


def generate_data(n_samples, flagc):
    # 3 grupe
    if flagc == 1:
        random_state = 365
        X,y = make_blobs(n_samples=n_samples, random_state=random_state)
    
    # 3 grupe
    elif flagc == 2:
        random_state = 148
        X,y = make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)

    # 4 grupe 
    elif flagc == 3:
        random_state = 148
        X, y = make_blobs(n_samples=n_samples,
                        centers = 4,
                        cluster_std=np.array([1.0, 2.5, 0.5, 3.0]),
                        random_state=random_state)
    # 2 grupe
    elif flagc == 4:
        X, y = make_circles(n_samples=n_samples, factor=.5, noise=.05)
    
    # 2 grupe  
    elif flagc == 5:
        X, y = make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X

# generiranje podatkovnih primjera
X = generate_data(500, 1)

# prikazi primjere u obliku dijagrama rasprsenja
plt.figure()
plt.scatter(X[:,0],X[:,1])
plt.xlabel('$x_1$')
plt.ylabel('$x_2$')
plt.title('podatkovni primjeri')
plt.show()

# 1)
# Postoje 3 grupe podataka.

# 2)
km = KMeans(n_clusters =3, init ='random', n_init =5 , random_state =0)
km.fit(X)
labels = km.predict(X)
plt.figure()
plt.scatter(X[:, 0], X[:, 1], c=labels, cmap='viridis', marker='o', edgecolor='k')
centers = km.cluster_centers_
plt.scatter(centers[:, 0], centers[:, 1], c='red', marker='x')
plt.xlabel('$x_1$')
plt.ylabel('$x_2$')
plt.title('podatkovni primjeri (1)')
plt.show()

# Promjenom K vrijednosti dobijemo vise grupa u kojima su podaci grupirani.
# Za ovaj nacin generiranja podataka optimalna vrijednost K je 3.
# 3)

# generiranje podatkovnih primjera (način 2)
X = generate_data(500, 2)
km = KMeans(n_clusters =3, init ='random', n_init =5 , random_state =0)
km.fit(X)
labels = km.predict(X)
plt.figure()
plt.scatter(X[:, 0], X[:, 1], c=labels, cmap='viridis', marker='o', edgecolor='k')
centers = km.cluster_centers_
plt.scatter(centers[:, 0], centers[:, 1], c='red', marker='x')
plt.xlabel('$x_1$')
plt.ylabel('$x_2$')
plt.title('podatkovni primjeri (2)')
plt.show()


# generiranje podatkovnih primjera (način 3)
X = generate_data(500, 3)
km = KMeans(n_clusters =4, init ='random', n_init =5 , random_state =0)
km.fit(X)
labels = km.predict(X)
plt.figure()
plt.scatter(X[:, 0], X[:, 1], c=labels, cmap='viridis', marker='o', edgecolor='k')
centers = km.cluster_centers_
plt.scatter(centers[:, 0], centers[:, 1], c='red', marker='x')
plt.xlabel('$x_1$')
plt.ylabel('$x_2$')
plt.title('podatkovni primjeri (3)')
plt.show()

# generiranje podatkovnih primjera (način 4)
X = generate_data(500, 4)
km = KMeans(n_clusters =2, init ='random', n_init =5 , random_state =0)
km.fit(X)
labels = km.predict(X)
plt.figure()
plt.scatter(X[:, 0], X[:, 1], c=labels, cmap='viridis', marker='o', edgecolor='k')
centers = km.cluster_centers_
plt.scatter(centers[:, 0], centers[:, 1], c='red', marker='x')
plt.xlabel('$x_1$')
plt.ylabel('$x_2$')
plt.title('podatkovni primjeri (4)')
plt.show()

# generiranje podatkovnih primjera (način 5)
X = generate_data(500, 5)
km = KMeans(n_clusters =2, init ='random', n_init =5 , random_state =0)
km.fit(X)
labels = km.predict(X)
plt.figure()
plt.scatter(X[:, 0], X[:, 1], c=labels, cmap='viridis', marker='o', edgecolor='k')
centers = km.cluster_centers_
plt.scatter(centers[:, 0], centers[:, 1], c='red', marker='x')
plt.xlabel('$x_1$')
plt.ylabel('$x_2$')
plt.title('podatkovni primjeri (5)')
plt.show()

# Najbolje rezultate nam daje sfericni dijagram zato sto su rezultati linerano odvojivi; jasno su definirane grupe i nema preklapanja.