import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans

# ucitaj sliku
img = Image.imread("imgs\\test_1.jpg")

# prikazi originalnu sliku
plt.figure()
plt.title("Originalna slika")
plt.imshow(img)
plt.tight_layout()
plt.show()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img = img.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w,h,d = img.shape
img_array = np.reshape(img, (w*h, d))

# rezultatna slika
img_array_aprox = img_array.copy()

# 1)
unique_colors = np.unique(img_array, axis=0)
print(f"Broj razlicitih boja u slici je: {len(unique_colors)}")

# 2)
kmeans = KMeans(n_clusters=5, random_state=0)
kmeans.fit(img_array_aprox)

# 3)
labels = kmeans.predict(img_array)
cluster_centers = kmeans.cluster_centers_
img_array_aprox = cluster_centers[labels].reshape(w, h, d)
plt.figure()
plt.title("Rezultantna slika nakon kvantizacije boja")
plt.imshow(img_array_aprox)
plt.tight_layout()
plt.show()

# 4)
# Mozemo primjetiti da je rezultatna slika mnogo jednostavnija od originalne odnosno slika ima ima samo onoliko boja kolika je vrijednost K.

# 5)  Primijenite postupak i na ostale dostupne slike.

# 6)
inertia_values = []
for k in range(1, 11):
    kmeans = KMeans(n_clusters=k, random_state=0)
    kmeans.fit(img_array)
    inertia_values.append(kmeans.inertia_)

plt.figure()
plt.plot(range(1, 11), inertia_values, marker='o')
plt.xlabel('Broj grupa K')
plt.ylabel('Inertia')
plt.title('Ovisnost Inertia o broju grupa K')
plt.show()

# Lakat se moze uociti kada je vrijenost K odnosno broj grupa 4.

# 7)
img_array_aprox = img_array_aprox.reshape(w, h, d)
index = 0
binary_image = np.zeros_like(img_array_aprox)
binary_image[np.all(img_array_aprox == cluster_centers[index], axis=-1)] = 1
plt.figure()
plt.title(f"Binarna slika za grupu {index}")
plt.imshow(binary_image, cmap='gray')
plt.tight_layout()
plt.show()

# Vidimo da su sve boje originalne slike osim onih koje odgovaraju odabranoj grupi prikazane crnom bojom, 
# dok su pikseli koji pripadaju odabranoj grupi prikazani bijelom bojom.