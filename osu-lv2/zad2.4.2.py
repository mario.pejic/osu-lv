import numpy as np
import matplotlib.pyplot as plt

data = np.genfromtxt('data.csv', delimiter=',')
broj_osoba = data.shape[0]
print("Na temelju veličine numpy polja data, na", broj_osoba, "osoba su izvršena mjerenja.")

plt.scatter(data[::, 2], data[::, 1], color='b', marker='.', s=5)
plt.xlabel('Masa (kg)')
plt.ylabel('Visina (cm)')
plt.title('Raspodjela mase i visine')

plt.scatter(data[::50, 2], data[::50, 1], color='b', marker='.', s=5)
plt.xlabel('Masa (kg)')
plt.ylabel('Visina (cm)')
plt.title('Raspodjela mase i visine')

visina = data[:, 1]

minimalna_visina = np.nanmin(visina)
maksimalna_visina = np.nanmax(visina)
srednja_visina = np.nanmean(visina)

print("Minimalna visina:", minimalna_visina, "cm")
print("Maksimalna visina:", maksimalna_visina, "cm")
print("Srednja visina:", srednja_visina, "cm")

ind_m = (data[:,0] == 1)
ind_z = (data[:,0] == 0)

data_m = data[ind_m]
data_z = data[ind_z]

print("Minimalna visina muškaraca:", np.nanmin(data_m[:, 1]), "cm")
print("Maksimalna visina muškaraca:", np.nanmax(data_m[:, 1]), "cm")
print("Srednja visina muškaraca:", np.nanmean(data_m[:, 1]), "cm")

print("Minimalna visina žena:", np.nanmin(data_z[:, 1]), "cm")
print("Maksimalna visina žena:", np.nanmax(data_z[:, 1]), "cm")
print("Srednja visina žena:", np.nanmean(data_z[:, 1]), "cm")

