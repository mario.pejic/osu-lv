import numpy as np
import matplotlib.pyplot as plt

black_square = np.zeros((50, 50))
white_square = np.ones((50, 50)) 
white_square *= 255  

top_row = np.hstack((black_square, white_square))
bottom_row = np.hstack((white_square, black_square))
checkered_board = np.vstack((top_row, bottom_row))

plt.imshow(checkered_board, cmap='gray', vmin=0, vmax=255)  
plt.show()