import numpy as np
import matplotlib.pyplot as plt

img = plt.imread("road.jpg")
brightened_img = img + [150, 150, 150]
plt.figure()
plt.imshow(brightened_img)

height, width , _= img.shape
second_quarter = img[:height // 2, width // 2:, :]
plt.figure ()
plt.imshow ( second_quarter )
plt.show ()

rotated_image = np.rot90(img, k=3)
plt.figure ()
plt.imshow ( rotated_image )
plt.show ()

mirrored_image = np.fliplr(img)
plt.figure ()
plt.imshow ( mirrored_image )
plt.show ()