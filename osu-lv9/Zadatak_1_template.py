import numpy as np
import tensorflow as tf
keras = tf.keras
layers = tf.keras.layers 
cifar10 = tf.keras.datasets.cifar10
to_categorical = tf.keras.utils.to_categorical
from matplotlib import pyplot as plt


# ucitaj CIFAR-10 podatkovni skup
(X_train, y_train), (X_test, y_test) = cifar10.load_data()

# prikazi 9 slika iz skupa za ucenje
plt.figure()
for i in range(9):
    plt.subplot(330 + 1 + i)
    plt.xticks([]),plt.yticks([])
    plt.imshow(X_train[i])

plt.show()


# pripremi podatke (skaliraj ih na raspon [0,1]])
X_train_n = X_train.astype('float32')/ 255.0
X_test_n = X_test.astype('float32')/ 255.0

# 1-od-K kodiranje
y_train = to_categorical(y_train)
y_test = to_categorical(y_test)

# CNN mreza
model = keras.Sequential()
model.add(layers.Input(shape=(32,32,3)))
model.add(layers.Conv2D(filters=32, kernel_size=(3, 3), activation='relu', padding='same'))
model.add(layers.MaxPooling2D(pool_size=(2, 2)))
model.add(layers.Conv2D(filters=64, kernel_size=(3, 3), activation='relu', padding='same'))
model.add(layers.MaxPooling2D(pool_size=(2, 2)))
model.add(layers.Conv2D(filters=128, kernel_size=(3, 3), activation='relu', padding='same'))
model.add(layers.MaxPooling2D(pool_size=(2, 2)))
model.add(layers.Flatten())
model.add(layers.Dense(500, activation='relu'))
model.add(layers.Dropout(0.3))
model.add(layers.Dense(10, activation='softmax'))

model.summary()

# definiraj listu s funkcijama povratnog poziva
my_callbacks = [
    keras.callbacks.TensorBoard(log_dir = 'logs/cnn_dropout',
                                update_freq = 100),
    keras.callbacks.EarlyStopping ( monitor ="val_loss",
                                        patience = 5,
                                        verbose = 1),
]

model.compile(optimizer='adam',
                loss='categorical_crossentropy',
                metrics=['accuracy'])

model.fit(X_train_n,
            y_train,
            epochs = 40,
            batch_size = 64,
            callbacks = my_callbacks,
            validation_split = 0.1)


score = model.evaluate(X_test_n, y_test, verbose=0)
print(f'Tocnost na testnom skupu podataka: {100.0*score[1]:.2f}')


# zad9.4.1.)
# 1)
# Ima 8 slojeva: 3 sloja kovolucijska sloja, 3 sloja sazminja i 2 potpuno povezana sloja.
# Mreza ima 1122758 parametara.

# 3)
# Krivulje koja pokazuje tocnost klasifikacije na trening skupu i skupu za validaciju imaju slicnu vrijednost do trece epohe; nakon toga vrijednost na trening skupu raste do
# tocnosti od 98.87, a vrijednost na validacijskom skupu do 75.3, dok na krivulja funkcije gubitka na trening skupu pada od vrijednosti 1.412 do 0.03703, a na validacijskom skupu
# raste od vrijednosti 1.106 do 2.324. To je zato sto je model preprilagoden za podatke na trening skupu te ima dosta vecu tocnost na trening skupu i dosta niske gubitke od 
# validacijskog skupa. 
# Tocnost na skupu za testiranje je 72.73.

# zad9.4.2.)
# Tocnost na testnom skupu je povecana, a tocnost trening skupa je nesto manja nego na proslom primjeru.

# zad9.4.3.)
# Dodajte funkciju povratnog poziva za rano zaustavljanje koja ce zaustaviti proces
# ucenja nakon što se 5 uzastopnih epoha ne smanji prosjecna vrijednost funkcije gubitka na validacijskom skupu.

# zad9.4.4.)
# 1) Jako velika velicina serije ucenja dovodi do brzeg ucenja, ali potrebno je vise racunalnih resursa zato ce svaka iteracija algoritma raditi s vecim brojem podataka.
#    Jako mala velicina serije moze dovesti do brzeg konvergiranja jer se cesce azuriraju tezine mreze, ali moze biti osjetljiva na sum 
#    u podacima i  moze zahtijevati dulje vrijeme ucenja.
# 2) Jako mala stopa ucenja: Moze dovesti do sporog konvergiranja ili zapinjanja u 
#    lokalnim minimumima jer su koraci azuriranja tezina mali.
#    Jako velika stopa ucenja: Moze uzrokovati divergenciju ili osciliranje u 
#    ucenju jer su koraci azuriranja preveliki i mogu "preskociti" minimum.
# 3) Ako se izbace odredeni slojevi iz mreze model losije uci slozene uzorke u podacima. Ako se izbace pretrenirani slojevi, model daje bolje rezultate. 
# 4) Smanjenje velicine skupa za ucenje za 50% dovodi do smanjenja raznolikosti u podacima i model losije generalizira.