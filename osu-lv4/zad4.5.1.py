from sklearn .model_selection import train_test_split
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
import sklearn.linear_model as lm
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score, calculate_mape

data = pd.read_csv('data_C02_emission.csv')

# a)
X = data[
    ['Engine Size (L)',
     'Cylinders',
     'Fuel Consumption City (L/100km)',
     'Fuel Consumption Hwy (L/100km)',
     'Fuel Consumption Comb (L/100km)',
     'Fuel Consumption Comb (mpg)']
]

y = data['CO2 Emissions (g/km)']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)

# b) 
plt.scatter(X_train['Engine Size (L)'], y_train, label='Training Data', color='blue')
plt.scatter(X_test['Engine Size (L)'], y_test, label='Test Data',  color='red')
plt.title('CO2 Emissions vs Engine Size')
plt.xlabel('Engine Size (L)')
plt.ylabel('CO2 Emissions (g/km)')
plt.legend()
plt.show()

# c)
sc = MinMaxScaler()
X_train_n = sc.fit_transform(X_train)

plt.figure()
plt.subplot(1, 2, 1)
plt.hist(X_train['Engine Size (L)'], bins=20, color='blue')
plt.title('Before Scaling')
plt.xlabel('Engine Size (L)')

plt.subplot(1, 2, 2)
plt.hist(X_train_n[: , 0], bins=20, color='blue')
plt.title('After Scaling')
plt.xlabel('Engine Size (L)')
plt.show()

X_test_n = sc.transform(X_test)

# d)
linearModel = lm.LinearRegression()
linearModel.fit(X_train_n, y_train)
print("Intercept:", linearModel.intercept_)
print("Coefficients:", linearModel.coef_)


# y^​(xi)= 182.235533540452 + 5.97412084⋅xi1​ + 69.73026306⋅xi2 − 60.46190471⋅xi3 ​− 3.70951856⋅xi4 ​+ 354.14025973⋅xi5 − 121.49620549⋅xi6
#           θ0                  θ1              θ2                  θ3              θ4              θ5                  θ6              

# e)
y_test_p = linearModel.predict(X_test_n)
plt.figure()
plt.scatter(y_test, y_test_p, color='blue')
plt.title('Actual vs. Predicted CO2 Emissions')
plt.xlabel('Actual CO2 Emissions (g/km)')
plt.ylabel('Predicted CO2 Emissions (g/km)')
plt.show()

# f)
MAE = mean_absolute_error(y_test, y_test_p)
MSE = mean_squared_error(y_test, y_test_p)
RMSE = np.sqrt(MSE)
MAPE = calculate_mape(y_test, y_test_p)
R2 = r2_score(y_test, y_test_p)
print("Mean Absolute Error (MAE):", MAE)
print("Mean Squared Error (MSE):", MSE)
print("Root Mean Squared Error (RMSE):", RMSE)
print("Mean Absolute Percentage Error (MAPE):", MAPE)
print("R-squared (R2) score:", R2)
