import pandas as pd
from sklearn.preprocessing import OneHotEncoder
from sklearn.linear_model import LinearRegression
from sklearn.metrics import max_error

data = pd.read_csv('data_C02_emission.csv')

ohe = OneHotEncoder ()
X_encoded = ohe.fit_transform(data[['Fuel Type']]).toarray()

X = data[['Engine Size (L)', 'Cylinders', 'Fuel Consumption City (L/100km)', 'Fuel Consumption Hwy (L/100km)', 'Fuel Consumption Comb (L/100km)', 'Fuel Consumption Comb (mpg)']]
X = pd.concat([pd.DataFrame(X_encoded, columns=ohe.get_feature_names_out(['Fuel Type'])), X], axis=1)
y = data['CO2 Emissions (g/km)']


linearModel = LinearRegression()
linearModel.fit(X, y)
y_pred = linearModel.predict(X)

max_error_value = max_error(y , y_pred)
print(f"Maksimalna pogreška u procjeni emisije CO2 plinova: {max_error_value}  g/km")

max_error_index = abs(y - y_pred).argmax()
vehicle_make = data.loc[max_error_index, 'Make']
vehicle_model = data.loc[max_error_index, 'Model']
print("Model vozila s maksimalnom pogreškom: {} {}".format(vehicle_make, vehicle_model))
