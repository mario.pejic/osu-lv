try:
  x = float(input("Unesi broj izmedu 0 i 1.0: "))
  if x >= 0.9 and x <= 1:
    print('A')
  elif x >= 0.8 and x < 0.9:
    print('B')  
  elif x >= 0.7 and x < 0.8:
    print('C')  
  elif x >= 0.6 and x < 0.7:
    print('D')
  elif x < 0.6:
    print('F')   
  else:
    print("Broj je izvan intervala.")     
except:
  print("Nije unesen broj.")