lista = []

def printList():
    print("Koliko je brojeva uneseno:", len(lista))
    print("Srednja vrijednost:", sum(lista)/len(lista))
    print("Minimalna vrijednost:", min(lista))
    print("Maksimalna vrijednost", max(lista))
    lista.sort()
    print("Sortirana lista:", (lista))


while True:
    num = input("Unesi Done za kraj programa, inace upisi broj: ")
    if num == "Done":
        printList()
        break
    else:
        try:
            num = float(num)
            lista.append(num)
        except:
            print("Upisati broj!")
            continue