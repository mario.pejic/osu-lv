import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

data = pd.read_csv('data_C02_emission.csv')

# a)
plt.figure()
plt.hist(data['CO2 Emissions (g/km)'], bins=20)
plt.title('CO2 Emissions (g/km)')
plt.show ()
# Najvise ima automobila koji emitiraju od 200 do 300 g/km, najmnaje automobila ima koji emitiraju preko 400 g/km.

# b)
diesel = data[data['Fuel Type'] == 'D']
gasoline = data[data['Fuel Type'] == 'X']
plt.scatter(diesel['Fuel Consumption City (L/100km)'], diesel['CO2 Emissions (g/km)'], color='blue', label='Diesel')
plt.scatter(gasoline['Fuel Consumption City (L/100km)'], gasoline['CO2 Emissions (g/km)'], color='red', label='Gasoline')
plt.xlabel('Fuel Consumption City (L/100km)')
plt.ylabel('CO2 Emissions (g/km)')
plt.title('Relationship between City Fuel Consumption and CO2 Emissions')
plt.show()
# Kod obje vrste motora emisija CO2 plinova linerano raste s obzirom na potrosnju goriva.

# c)
data.boxplot(column='Fuel Consumption Hwy (L/100km)', by='Fuel Type')
plt.xlabel('Fuel Type')
plt.ylabel('Fuel Consumption Hwy (L/100km)')
plt.title('Distribution of Highway Fuel Consumption by Fuel Type')
plt.show()
# Kod premium goriva ima dosta vozila iznad maksimalne teoretske potrosnje goriva.

# d)
fuel_type_counts = data.groupby('Fuel Type').size()
fuel_type_counts.plot(kind='bar', color='blue', edgecolor='black')
plt.title('Number of Vehicles for each Fuel Type')
plt.xlabel('Fuel Type')
plt.ylabel('NUmber of Vehicles')
plt.show()

# e)
avg_co2_by_cylinders = data.groupby('Cylinders')['CO2 Emissions (g/km)'].mean()
avg_co2_by_cylinders.plot(kind='bar', color='Blue', edgecolor='black')
plt.title('Average CO2 Emission for Each Cylinder Group')
plt.xlabel('Number of Cylinders')
plt.ylabel('Average CO2 Emission (g/km)')
plt.show()