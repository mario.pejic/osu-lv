import pandas as pd
import numpy as np

data = pd.read_csv('osu-lv3/data_C02_emission.csv')

# a)
number_of_measurments = data.shape[0]
print("Number_of_measurments: " + str(number_of_measurments))

types_of_measurments = data.dtypes
print("Types_of_measurments" + str(types_of_measurments))

duplicates = data.duplicated().sum()
print("Duplicates: " + str(duplicates))

null_data = data.isnull().sum().sum()
print("Null_data: " + str(null_data))

data.dropna(axis=0)
data.drop_duplicates()
data = data.reset_index(drop = True)
data['Make'] = data['Make'].astype('category')
data['Model'] = data['Model'].astype('category')
data['Vehicle Class'] = data['Vehicle Class'].astype('category')
data['Transmission'] = data['Transmission'].astype('category')
data['Fuel Type'] = data['Fuel Type'].astype('category')

# b)
highest_city_consumtion = data.sort_values(by = 'Fuel Consumption City (L/100km)').head(3)[['Make', 'Model', 'Fuel Consumption City (L/100km)']]
print("Highest city consumption: " + str(highest_city_consumtion))

lowest_city_consumtion = data.sort_values(by = 'Fuel Consumption City (L/100km)').tail(3)[['Make', 'Model', 'Fuel Consumption City (L/100km)']]
print("Lowest city consumption: " + str(lowest_city_consumtion))

# c)
motor_size = data[(data['Engine Size (L)'] > 2.5) & (data['Engine Size (L)'] < 3.5)]
print("Numbers of vehicles with engine size between 2.5L and 3.5L: " + str(motor_size.shape[0]))
print("Average CO2 emission of vehicles with engine size between 2.5L and 3.5L: " + str(motor_size['CO2 Emissions (g/km)'].mean()))

# d)
audi_vehicles = data[data['Make'] == 'Audi']
cylinder4_audis = audi_vehicles[audi_vehicles['Cylinders']==4] 
print("Number of audis: " + str(audi_vehicles.shape[0]))
print("CO2 emission of 4 cylinder audis: " + str(cylinder4_audis['CO2 Emissions (g/km)'].mean()))

# e)
print("Number of vehicles with each number of cylinders:\n" + str(data['Cylinders'].value_counts()))
print("CO2 emisson depending on groups with different number of cylinders:\n" + str(data['Cylinders'].value_counts()) + str(data.groupby('Cylinders')['CO2 Emissions (g/km)'].mean()))

# f) 
diesel = data[data['Fuel Type'] == 'D']
gasoline = data[data['Fuel Type'] == 'X']
print("City consumption od diesels:\n" + str(diesel['Fuel Consumption City (L/100km)'].mean()))
print("City consumption od gasolines:\n" + str(gasoline['Fuel Consumption City (L/100km)'].mean()))
print("Diesel median:\n" + str(diesel['Fuel Consumption City (L/100km)'].median()))
print("gasoline median:\n" + str(gasoline['Fuel Consumption City (L/100km)'].median()))

# g)
print("Highest diesel 4 cylinder fuel consumption vehicle:\n" + str(data[(data['Cylinders']==4) & (data['Fuel Type']=='D')].sort_values(by='Fuel Consumption City (L/100km)', ascending=False).head(1)))

# h)
print("Number of manuals:\n" + str(data[data['Transmission'].str.startswith('M')].shape[0]))

# i)
print(data.corr(numeric_only = True))