import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay, classification_report

X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                            random_state=213, n_clusters_per_class=1, class_sep=1)

# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)

# a)
plt.figure()
plt.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap='coolwarm', label='Train Data')
plt.scatter(X_test[:, 0], X_test[:, 1], c=y_test, cmap='coolwarm', marker='x', label='Test Data')
plt.title('Train and Test Data Visualization')
plt.xlabel('x1')
plt.ylabel('x2')
plt.legend()
plt.show()

# b)
LogRegModel = LogisticRegression(random_state=0)
LogRegModel.fit(X_train, y_train)

# c)
theta1, theta2 = LogRegModel.coef_[0]
theta0 = LogRegModel.intercept_[0]

x1_values = np.linspace(X_train[:, 0].min(), X_train[:, 0].max(), 100)
x2_values = -(theta1 * x1_values + theta0) / theta2

plt.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap='coolwarm', label='Train Data')
plt.plot(x1_values, x2_values, label='Decision Boundary', color='black')
plt.title('Train Data with Decision Boundary')
plt.xlabel('x1')
plt.ylabel('x2')
plt.legend()
plt.show()

# d)
y_pred = LogRegModel.predict(X_test)

conf_matrix = confusion_matrix(y_test, y_pred)
disp = ConfusionMatrixDisplay(confusion_matrix=conf_matrix)
disp.plot(cmap=plt.cm.Blues)
plt.show()

report = classification_report(y_test, y_pred)
print(report)

# e)
correctly_classified = y_test == y_pred
incorrectly_classified = ~correctly_classified

plt.figure(figsize=(8, 6))
plt.scatter(X_test[correctly_classified, 0], X_test[correctly_classified, 1], color='green', label='Correctly Classified', edgecolor='k')
plt.scatter(X_test[incorrectly_classified, 0], X_test[incorrectly_classified, 1], color='red', label='Incorrectly Classified', edgecolor='k')
plt.title('Test Data Classification')
plt.xlabel('x1')
plt.ylabel('x2')
plt.legend()
plt.show()