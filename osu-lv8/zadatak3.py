import numpy as np
import keras as k
from keras import layers, utils
from matplotlib import pyplot as plt
from sklearn.metrics import ConfusionMatrixDisplay, confusion_matrix

image = utils.load_img('dva.png', target_size = (28, 28), color_mode = "grayscale")
image = utils.img_to_array(image)
image_s = image.astype("float32") / 255
image_s = np.expand_dims(image_s, -1)
image_s = image_s.reshape(1, 784)

model = k.saving.load_model('osu-lv8/FCN/')
predicted = model.predict(image_s)

print("Predicted value is ", predicted.argmax())
